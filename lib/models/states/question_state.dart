import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:latlng/latlng.dart';
import 'package:meta/meta.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';

part 'question_state.g.dart';

@immutable
@HiveType(typeId: 1)
class QuestionState extends Equatable {
  final Question? currentQuestion;
  final double currentScore;
  final int currentQuestionNum;
  final List<QuestionType> validCategories;
  final bool showingSolution;
  final LatLng? guessedLocation;
  final int? sc_selectedIndex;

  // TODO: Maybe change hardcoded validCategories to dynamic magic
  const QuestionState(
      {this.currentQuestion = null,
      this.currentScore = 0,
      this.currentQuestionNum = 0,
      this.showingSolution = false,
      this.guessedLocation = null,
      this.sc_selectedIndex = null,
      this.validCategories = const [
        QuestionType.geoguesser,
        QuestionType.guess_value,
        QuestionType.single_choice
      ]});

  QuestionState copyWith({
    Question? currentQuestion,
    List<QuestionType>? validCategories,
    double? currentScore,
    int? currentQuestionNum,
    int? sc_selectedIndex,
    LatLng? guessedLocation,
    bool? showingSolution,
  }) {
    return QuestionState(
      validCategories: validCategories ?? this.validCategories,
      currentScore: currentScore ?? this.currentScore,
      currentQuestionNum: currentQuestionNum ?? this.currentQuestionNum,
      currentQuestion: currentQuestion ?? this.currentQuestion,
      guessedLocation: guessedLocation ?? this.guessedLocation,
      sc_selectedIndex: sc_selectedIndex ?? this.sc_selectedIndex,
      showingSolution: showingSolution ?? this.showingSolution,
    );
  }

  QuestionState copyWithNullableGuessedLocation(
      {required LatLng? guessedLocation, required int? sc_selectedIndex}) {
    return QuestionState(
      validCategories: this.validCategories,
      currentScore: this.currentScore,
      currentQuestionNum: this.currentQuestionNum,
      currentQuestion: this.currentQuestion,
      guessedLocation: guessedLocation,
      sc_selectedIndex: sc_selectedIndex,
      showingSolution: this.showingSolution,
    );
  }

  @override
  List<Object?> get props => [
        currentQuestion,
        currentScore,
        currentQuestionNum,
        validCategories,
        showingSolution,
        guessedLocation,
        sc_selectedIndex
      ];
}
