import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:meta/meta.dart';
import 'package:tenderwatchapp/models/app_environment.dart';
import 'package:tenderwatchapp/models/states/question_state.dart';

part 'app_state.g.dart';

@immutable
@HiveType(typeId: 0)
class AppState extends Equatable {
  // Newest DB version num - manually set
  static const int CUR_DB_VERSION_NUM = 1;

  @HiveField(0)
  final int dbVersionNum;

  @HiveField(1)
  final int counter;

  @HiveField(2)
  final QuestionState questionState;

  // Doesn't get saved
  final AppEnvironment? appEnvironment;

  const AppState(
      {this.appEnvironment,
      this.dbVersionNum = CUR_DB_VERSION_NUM,
      this.counter = 0,
      this.questionState = const QuestionState()});

  AppState copyWith(
      {int? counter,
      QuestionState? questionState,
      AppEnvironment? appEnvironment,
      int? dbVersionNum}) {
    return AppState(
      counter: counter ?? this.counter,
      questionState: questionState ?? this.questionState,
      dbVersionNum: dbVersionNum ?? this.dbVersionNum,
      appEnvironment: appEnvironment ?? this.appEnvironment,
    );
  }

  @override
  List<Object?> get props =>
      [counter, dbVersionNum, appEnvironment, questionState];
}
