import 'package:tenderwatchapp/models/data/question_types/question.dart';

class GuessQuestion extends Question {
  double answer;

  GuessQuestion(
      {required category,
      required openDataUrl,
      required questionText,
      required image,
      required this.answer,
      String? imageSrc,
      String? openDataDesc,
      String? visualisueringLink})
      : super(
            type: QuestionType.guess_value,
            category: category,
            openDataUrl: openDataUrl,
            questionText: questionText,
            image: image,
            imageSrc: imageSrc,
            openDataDesc: openDataDesc,
            visualisueringLink: visualisueringLink) {}

  @override
  List<Object?> get props => super.props
    ..addAll([
      answer,
    ]);
}
