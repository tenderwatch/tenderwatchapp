import 'package:tenderwatchapp/models/data/question_types/question.dart';

class SingleChoiceQuestion extends Question {
  final List<String> options;
  final int correct_answer;

  SingleChoiceQuestion({
    required category,
    required openDataUrl,
    required questionText,
    required image,
    required this.options,
    required this.correct_answer,
    String? imageSrc,
    String? openDataDesc,
    String? visualisueringLink,
  }) : super(
          type: QuestionType.single_choice,
          category: category,
          openDataUrl: openDataUrl,
          questionText: questionText,
          image: image,
          imageSrc: imageSrc,
          visualisueringLink: visualisueringLink,
        ) {}

  @override
  List<Object?> get props => super.props
    ..addAll([
      options,
      correct_answer,
    ]);
}
