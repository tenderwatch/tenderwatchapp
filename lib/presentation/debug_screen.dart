import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/geoguesser_map_widget.dart';

class DebugScreen extends StatefulWidget {
  @override
  DebugScreenState createState() => DebugScreenState();
}

class DebugScreenState extends State<DebugScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(height: 200),
          Container(
            height: MediaQuery.of(context).size.height - 200,
            child: GeoguesserMapWidget(),
          ),
        ],
      ),
    );
  }
}
