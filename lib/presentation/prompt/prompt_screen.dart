import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:tenderwatchapp/app.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/answer_box.dart';
import 'package:tenderwatchapp/presentation/prompt/question_box.dart';
import 'package:tenderwatchapp/presentation/widgets/full_screen_image/full_screen_capable_image.dart';

class PromptScreen extends StatelessWidget {
  final double heightFactorTop = 0.5;

  @override
  Widget build(BuildContext context) {
    final viewInsets = EdgeInsets.fromWindowPadding(
      WidgetsBinding.instance!.window.viewInsets,
      WidgetsBinding.instance!.window.devicePixelRatio,
    );
    final screenHeight = min(
      MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
      1000,
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: TenderWatchAppState.backgroundColor,
      body: Center(
        child: StoreConnector<AppState, Question?>(
          converter: (store) => store.state.questionState.currentQuestion,
          builder: (context, Question? currentQuestion) {
            return Container(
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: 1200,
                ),
                child: Column(
                  children: [
                    SizedBox(height: screenHeight * 0.05),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 10),
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(25)),
                                  child: Container(
                                    constraints: BoxConstraints(
                                      maxHeight: screenHeight * 0.16,
                                    ),
                                    child: FullScreenCapableImage(
                                      image: Image.network(
                                        currentQuestion!.image,
                                      ).image,
                                    ),
                                  ),
                                ),
                              ),
                              Chip(
                                label: Text(currentQuestion.category),
                                backgroundColor: TenderWatchAppState.backgroundColor,
                              ),
                              Container(
                                height: screenHeight * 0.18,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: QuestionBox(
                                    currentQuestion != null
                                        ? currentQuestion.questionText
                                        : "",
                                  ),
                                ),
                              ),
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.46,
                                child: AnswerBox(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
