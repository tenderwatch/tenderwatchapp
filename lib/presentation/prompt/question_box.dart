import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class QuestionBox extends StatelessWidget {
  final String _title;

  QuestionBox(this._title);

  Widget build(BuildContext context) {
    return Text(
      this._title,
      style: TextStyle(fontSize: 20),
      textAlign: TextAlign.center,
      maxLines: 5,
      overflow: TextOverflow.ellipsis,
    );
  }
}
