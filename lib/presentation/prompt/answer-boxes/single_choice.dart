import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:tenderwatchapp/actions/actions.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';
import 'package:tenderwatchapp/models/data/question_types/single_choice_question.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/cards/answer_card.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/cards/result_card.dart';
import 'package:tuple/tuple.dart';

import 'answer_box_template.dart';

class SingleChoice extends StatefulWidget implements AnswerBoxTemplate {
  int? selectedOptionIdx;

  @override
  _SingleChoiceState createState() => _SingleChoiceState();

  @override
  double calculateScore(Question question) {
    if (selectedOptionIdx ==
        (question as SingleChoiceQuestion).correct_answer) {
      return 10;
    } else {
      return 0;
    }
  }
}

class _SingleChoiceState extends State<SingleChoice> {
  static StatelessWidget buildCard(final String text, final int correct_option,
      final int idx, final int? selectedOptionIdx) {
    var isSelected = selectedOptionIdx != null && idx == selectedOptionIdx;
    bool? correctSelection = null;
    if (isSelected) {
      correctSelection = correct_option == selectedOptionIdx;
    } else if (correct_option == idx) {
      correctSelection = true;
    }

    return StoreConnector<AppState, Tuple2<bool, void Function(int?)>>(
        converter: (store) => Tuple2(store.state.questionState.showingSolution,
            (selIdx) => store.dispatch(SetSCSelectedAnswerIdxAction(selIdx))),
        builder: (context, display_resultsAndSelectTuple) {
          return display_resultsAndSelectTuple.item1
              ? ResultCard(titleLabel: text, isCorrect: correctSelection)
              : AnswerCard(
                  titleLabel: text,
                  isSelected: isSelected,
                  onTap: () => toggleSelectedQuestion(idx, selectedOptionIdx,
                      display_resultsAndSelectTuple.item2));
        });
  }

  static void toggleSelectedQuestion(
      int idx, int? selectedOptionIdx, void Function(int? idx) setSelectedIdx) {
    if (selectedOptionIdx != null && selectedOptionIdx == idx) {
      // toggle current to unselected
      setSelectedIdx(null);
    } else {
      setSelectedIdx(idx);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Tuple2<Question?, int?>>(
      converter: (store) => Tuple2(store.state.questionState.currentQuestion,
          store.state.questionState.sc_selectedIndex),
      builder: (context, t) {
        Question? question = t.item1;
        int? selectedOptionIdx = t.item2;

        // TODO: dont update stateless widget here
        widget.selectedOptionIdx = selectedOptionIdx;

        var sc_question = (question as SingleChoiceQuestion);
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
              children: sc_question.options
                  .asMap()
                  .entries
                  .map((e) => buildCard(e.value, sc_question.correct_answer,
                      e.key, selectedOptionIdx))
                  .toList()
                  .fold([], (pre, ele) {
            if (pre == []) {
              return [ele];
            }
            return [...pre, SizedBox(height: 8), ele];
          })),
        );
      },
    );
  }
}
