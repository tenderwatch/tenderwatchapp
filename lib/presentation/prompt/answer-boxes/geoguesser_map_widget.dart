import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlng/latlng.dart';
import 'package:map/map.dart';
import 'package:tenderwatchapp/actions/actions.dart';
import 'package:tenderwatchapp/models/data/question_types/geoguesser_question.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/answer_box_template.dart';
import 'package:tuple/tuple.dart';

class GeoguesserMapWidget extends StatefulWidget implements AnswerBoxTemplate {
  LatLng? currentCenterLocation;

  @override
  GeoguesserMapWidgetState createState() => GeoguesserMapWidgetState();

  // TODO: Change GeoGuesser score calculation
  @override
  double calculateScore(Question question) {
    final correctLocation = (question as GeoguesserQuestion).coords;
    final guessedLocation = currentCenterLocation!;

    return 10 - min(calculateDistance(guessedLocation, correctLocation) / 100,  10);
  }

  static double calculateDistance(LatLng loc1, LatLng loc2) {
    return Geolocator.distanceBetween(
      loc1.latitude,
      loc1.longitude,
      loc2.latitude,
      loc2.longitude,
    ).roundToDouble();
  }
}

class GeoguesserMapWidgetState extends State<GeoguesserMapWidget> {
  static LatLng startCoordinates = LatLng(
    51.9615884,
    7.6265888,
  ); // Map starting center point (Currently Münster Town Hall)

  final controller = MapController(
    location: startCoordinates,
  );

  Offset? _dragStart;
  double _scaleStart = 1.0;
  bool _darkMode = false;
  bool hasShownSolution = false;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState,
        Tuple3<GeoguesserQuestion?, bool, void Function(LatLng)>>(
      converter: (store) => Tuple3(
        store.state.questionState.currentQuestion as GeoguesserQuestion,
        store.state.questionState.showingSolution,
        (guessedLocation) =>
            store.dispatch(SetGuessedLocationAction(guessedLocation)),
      ),
      builder: (context, questionAndShowingSolutionTuple) {
        return Column(
          children: [
            Expanded(
              child: MapLayoutBuilder(
                controller: controller,
                builder: (context, transformer) {
                  widget.currentCenterLocation = controller.center;

                  final centerLocation = Offset(
                    transformer.constraints.biggest.width / 2,
                    transformer.constraints.biggest.height / 2,
                  );

                  final centerMarkerWidget =
                      _buildMarkerWidget(centerLocation, Colors.purple);

                  Widget solutionMarkerWidget = SizedBox();
                  if (questionAndShowingSolutionTuple.item2) {
                    if (!hasShownSolution) {
                      hasShownSolution = true;
                      questionAndShowingSolutionTuple.item3(controller.center);
                      controller.center =
                          questionAndShowingSolutionTuple.item1!.coords;
                    }

                    solutionMarkerWidget = _buildMarkerWidget(
                      transformer.fromLatLngToXYCoords(
                        questionAndShowingSolutionTuple.item1!.coords,
                      ),
                      Colors.red,
                    );
                  }

                  return GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onDoubleTap: _onDoubleTap,
                    onScaleStart: _onScaleStart,
                    onScaleUpdate: _onScaleUpdate,
                    onTapUp: (details) {
                      final location = transformer
                          .fromXYCoordsToLatLng(details.localPosition);
                      final clicked =
                          transformer.fromLatLngToXYCoords(location);
                    },
                    child: Listener(
                      behavior: HitTestBehavior.opaque,
                      onPointerSignal: (event) {
                        if (event is PointerScrollEvent) {
                          final delta = event.scrollDelta;

                          controller.zoom -= delta.dy / 1000.0;
                          setState(() {});
                        }
                      },
                      child: Stack(
                        children: [
                          Map(
                            controller: controller,
                            builder: (context, x, y, z) {
                              // TODO: Is this legally relevant for our purposes? Probably should be switched out when going in production
                              //Legal notice: This url is only used for demo and educational purposes. You need a license key for production use.

                              //Google Maps
                              final url =
                                  'https://www.google.com/maps/vt/pb=!1m4!1m3!1i$z!2i$x!3i$y!2m3!1e0!2sm!3i420120488!3m7!2sen!5e1105!12m4!1e68!2m2!1sset!2sRoadmap!4e0!5m1!1e0!23i4111425';

                              final darkUrl =
                                  'https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i$z!2i$x!3i$y!4i256!2m3!1e0!2sm!3i556279080!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcC52Om9uLHMuZTpsfHAudjpvZmZ8cC5zOi0xMDAscy5lOmwudC5mfHAuczozNnxwLmM6I2ZmMDAwMDAwfHAubDo0MHxwLnY6b2ZmLHMuZTpsLnQuc3xwLnY6b2ZmfHAuYzojZmYwMDAwMDB8cC5sOjE2LHMuZTpsLml8cC52Om9mZixzLnQ6MXxzLmU6Zy5mfHAuYzojZmYwMDAwMDB8cC5sOjIwLHMudDoxfHMuZTpnLnN8cC5jOiNmZjAwMDAwMHxwLmw6MTd8cC53OjEuMixzLnQ6NXxzLmU6Z3xwLmM6I2ZmMDAwMDAwfHAubDoyMCxzLnQ6NXxzLmU6Zy5mfHAuYzojZmY0ZDYwNTkscy50OjV8cy5lOmcuc3xwLmM6I2ZmNGQ2MDU5LHMudDo4MnxzLmU6Zy5mfHAuYzojZmY0ZDYwNTkscy50OjJ8cy5lOmd8cC5sOjIxLHMudDoyfHMuZTpnLmZ8cC5jOiNmZjRkNjA1OSxzLnQ6MnxzLmU6Zy5zfHAuYzojZmY0ZDYwNTkscy50OjN8cy5lOmd8cC52Om9ufHAuYzojZmY3ZjhkODkscy50OjN8cy5lOmcuZnxwLmM6I2ZmN2Y4ZDg5LHMudDo0OXxzLmU6Zy5mfHAuYzojZmY3ZjhkODl8cC5sOjE3LHMudDo0OXxzLmU6Zy5zfHAuYzojZmY3ZjhkODl8cC5sOjI5fHAudzowLjIscy50OjUwfHMuZTpnfHAuYzojZmYwMDAwMDB8cC5sOjE4LHMudDo1MHxzLmU6Zy5mfHAuYzojZmY3ZjhkODkscy50OjUwfHMuZTpnLnN8cC5jOiNmZjdmOGQ4OSxzLnQ6NTF8cy5lOmd8cC5jOiNmZjAwMDAwMHxwLmw6MTYscy50OjUxfHMuZTpnLmZ8cC5jOiNmZjdmOGQ4OSxzLnQ6NTF8cy5lOmcuc3xwLmM6I2ZmN2Y4ZDg5LHMudDo0fHMuZTpnfHAuYzojZmYwMDAwMDB8cC5sOjE5LHMudDo2fHAuYzojZmYyYjM2Mzh8cC52Om9uLHMudDo2fHMuZTpnfHAuYzojZmYyYjM2Mzh8cC5sOjE3LHMudDo2fHMuZTpnLmZ8cC5jOiNmZjI0MjgyYixzLnQ6NnxzLmU6Zy5zfHAuYzojZmYyNDI4MmIscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6NnxzLmU6bC50fHAudjpvZmYscy50OjZ8cy5lOmwudC5mfHAudjpvZmYscy50OjZ8cy5lOmwudC5zfHAudjpvZmYscy50OjZ8cy5lOmwuaXxwLnY6b2Zm!4e0&key=AIzaSyAOqYYyBbtXQEtcHG7hwAwyCPQSYidG8yU&token=31440';
                              //Mapbox Streets
                              // final url =
                              //     'https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/$z/$x/$y?access_token=YOUR_MAPBOX_ACCESS_TOKEN';

                              return CachedNetworkImage(
                                imageUrl: _darkMode ? darkUrl : url,
                                fit: BoxFit.cover,
                              );
                            },
                          ),
                          solutionMarkerWidget,
                          StoreConnector<AppState, LatLng?>(
                            converter: (store) =>
                                store.state.questionState.guessedLocation,
                            builder: (context, guessedLocation) {
                              return guessedLocation != null
                                  ? _buildMarkerWidget(
                                      transformer.fromLatLngToXYCoords(
                                        guessedLocation,
                                      ),
                                      Colors.purple,
                                    )
                                  : SizedBox();
                            },
                          ),
                          if (!questionAndShowingSolutionTuple.item2)
                            centerMarkerWidget,
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            (!questionAndShowingSolutionTuple.item2)
                ? SizedBox()
                : StoreConnector<AppState, LatLng?>(
                    converter: (store) =>
                        store.state.questionState.guessedLocation,
                    builder: (context, guessedLocation) {
                      return Text('geoguesser.solution_distance_label')
                          .tr(args: [
                        GeoguesserMapWidget.calculateDistance(
                          questionAndShowingSolutionTuple.item1!.coords,
                          guessedLocation ?? controller.center,
                        ).toString(),
                      ]);
                    },
                  ),
          ],
        );
      },
    );
  }

  LatLng getCurrentCenter() {
    return controller.center;
  }

  void _gotoDefault() {
    controller.center = startCoordinates;
    setState(() {});
  }

  void _onDoubleTap() {
    controller.zoom += 0.5;
    setState(() {});
  }

  void _onScaleStart(ScaleStartDetails details) {
    _dragStart = details.focalPoint;
    _scaleStart = 1.0;
  }

  void _onScaleUpdate(ScaleUpdateDetails details) {
    final scaleDiff = details.scale - _scaleStart;
    _scaleStart = details.scale;

    if (scaleDiff > 0) {
      controller.zoom += 0.02;
      setState(() {});
    } else if (scaleDiff < 0) {
      controller.zoom -= 0.02;
      setState(() {});
    } else {
      final now = details.focalPoint;
      final diff = now - _dragStart!;
      _dragStart = now;
      controller.drag(diff.dx, diff.dy);
      setState(() {});
    }
  }

  Widget _buildMarkerWidget(Offset pos, Color color) {
    return Positioned(
      left: pos.dx - 16,
      top: pos.dy - 16,
      width: 24,
      height: 24,
      child: Icon(Icons.location_on, color: color),
    );
  }
}
