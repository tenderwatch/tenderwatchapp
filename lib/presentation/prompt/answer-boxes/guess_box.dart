import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:tenderwatchapp/models/data/question_types/guess_question.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/answer_box_template.dart';
import 'package:tuple/tuple.dart';

class GuessBox extends StatefulWidget implements AnswerBoxTemplate {
  double curInput = 0;

  @override
  GuessBoxState createState() => GuessBoxState();

  @override
  double calculateScore(Question question) {
    double correctAnswer = (question as GuessQuestion).answer;
    return min((100 - min((correctAnswer - curInput).abs(), 100)) / 10, 10);
  }
}

class GuessBoxState extends State<GuessBox> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Tuple2<Question?, bool>>(
      converter: (store) => Tuple2(
        store.state.questionState.currentQuestion,
        store.state.questionState.showingSolution,
      ),
      builder: (context, questionAndShowingSolution) {
        return Column(
          children: [
            Padding(
              padding:
                  EdgeInsets.only(left: 100, right: 100),
              child: TextFormField(
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 40),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                // Only numbers can be entered
                initialValue: "0",
                enabled: !questionAndShowingSolution.item2,
                onChanged: (val) {
                  if (val == '') {
                    widget.curInput = 0;
                    return;
                  }

                  widget.curInput = double.parse(val);
                },
              ),
            ),
            if (questionAndShowingSolution.item2)
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  'guess.solution_label',
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ).tr(args: [
                  (questionAndShowingSolution.item1! as GuessQuestion)
                      .answer
                      .toString(),
                ]),
              ),
          ],
        );
      },
    );
  }
}
