import 'package:flutter/material.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';

abstract class AnswerBoxTemplate extends Widget {
  double calculateScore(Question question);
}
