import 'package:simple_rich_text/simple_rich_text.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:get_it/get_it.dart';
import 'package:tenderwatchapp/actions/actions.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/answer_box_template.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/geoguesser_map_widget.dart';
import 'package:tenderwatchapp/routes.dart';
import 'package:tenderwatchapp/services/questions/question_service.dart';
import 'package:tuple/tuple.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:tenderwatchapp/presentation/prompt/answer-boxes/single_choice.dart';
import 'guess_box.dart';

class AnswerBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Question?>(
      converter: (store) => store.state.questionState.currentQuestion,
      builder: (context, Question? question) {
        if (question == null) {
          return SizedBox();
        }

        AnswerBoxTemplate? answerWidget = null;
        switch (question.type) {
          case QuestionType.geoguesser:
            answerWidget = GeoguesserMapWidget();
            break;
          case QuestionType.guess_value:
            answerWidget = GuessBox();
            break;
          case QuestionType.single_choice:
            answerWidget = SingleChoice();
            break;
        }

        if (answerWidget == null) {
          return Container();
        }

        return Center(
          child: Column(
            children: [
              SizedBox(height: 0),
              Expanded(
                child: Center(
                  child: answerWidget,
                ),
              ),
              Container(
                child: Padding(
                  padding: EdgeInsets.only(top: 0),
                  child: StoreConnector<
                      AppState,
                      Tuple5<bool, void Function(double), void Function(),
                          void Function(), int>>(
                    converter: (store) => Tuple5(
                      store.state.questionState.showingSolution,
                      (score) => store.dispatch(SetCurrentScoreAction(
                        store.state.questionState.currentScore + score,
                      )),
                      () => store.dispatch(SwitchToNextQuestionAction(
                          store.state.questionState.currentQuestionNum + 1)),
                      () => store.dispatch(SetShowingSolutionAction(true)),
                      store.state.questionState.currentQuestionNum,
                    ),
                    builder: (context, storeTuple) {
                      final questionService = GetIt.I.get<QuestionService>();
                      final isLastQuestion = storeTuple.item5 ==
                          questionService.allQuestions.length;
                      return StoreConnector<AppState, Tuple2<double, int>>(
                        converter: (store) => Tuple2(
                          store.state.questionState.currentScore,
                          store.state.questionState.currentQuestionNum,
                        ),
                        builder: (context, scoreAndQuestionSize) {
                          if (storeTuple.item1) {
                            // showing solution
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 10,
                                  ),
                                  child: ElevatedButton(
                                    onPressed: () {
                                      showQuestionInfo(context);
                                    },
                                    child: Text(
                                      'questions.question_info_button',
                                    ).tr(),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 10,
                                  ),
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      if (!isLastQuestion) {
                                        storeTuple.item3();
                                      } else {
                                        await showQuizOverDialog(
                                            context,
                                            scoreAndQuestionSize.item1,
                                            scoreAndQuestionSize.item2);
                                      }
                                    },
                                    child: Text(
                                      isLastQuestion
                                          ? 'questions.last_question_button'
                                          : 'questions.next_question_button',
                                    ).tr(),
                                  ),
                                ),
                              ],
                            );
                          }

                          return ElevatedButton(
                            onPressed: () {
                              storeTuple.item2(
                                  answerWidget!.calculateScore(question));
                              storeTuple.item4();
                            },
                            child: Text(
                              'questions.show_solution_button',
                            ).tr(),
                          );
                        },
                      );
                    },
                  ),
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        );
      },
    );
  }

  Future<void> showQuizOverDialog(
      BuildContext context, double score, int questionSize) async {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('prompt.quiz_end_dialog.heading').tr(),
          content: Text(
            'prompt.quiz_end_dialog.body',
          ).tr(args: [
            score.round().toString(),
            (10 * questionSize).round().toString(),
          ]),
          actions: [
            TextButton(
              child: Text('prompt.quiz_end_dialog.ok_button').tr(),
              onPressed: () {
                Navigator.of(context)
                    .popUntil(ModalRoute.withName(Routes.home));
              },
            ),
            TextButton(
              child: Text('prompt.quiz_end_dialog.god_button').tr(),
              onPressed: () {
                Navigator.of(context)
                    .popUntil(ModalRoute.withName(Routes.home));
              },
            ),
          ],
        );
      },
    );
  }

  void showQuestionInfo(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (context) {
          return StoreConnector<AppState, Question?>(
            converter: (store) => store.state.questionState.currentQuestion,
            builder: (context, question) {
              if (question == null) {
                return SizedBox();
              }
              return Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("questions.info.header",
                            style: TextStyle(fontSize: 25))
                        .tr(),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.network(
                          "https://www.kaiserhof-muenster.de/site/assets/files/3774/vorschaubild_staedtereisen.jpg"),
                    ),
                    StoreConnector<AppState, Question?>(
                      converter: (store) =>
                          store.state.questionState.currentQuestion,
                      builder: (context, currentQuestion) {
                        return RichText(
                            text: TextSpan(
                                style: DefaultTextStyle.of(context).style,
                                children: currentQuestion == null
                                    ? []
                                    : getRichttexts(currentQuestion)));
                      },
                    ),
                  ],
                ),
              );
            },
          );
        });
  }

  List<TextSpan> getRichttexts(Question currentQuestion) {
    List<TextSpan> spans = [];
    spans.add(TextSpan(text: "Die hier genutzte Quelle findest du "));
    spans.add(TextSpan(
        text: 'hier',
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue),
        recognizer: new TapGestureRecognizer()
          ..onTap = () {
            launch(currentQuestion.openDataUrl);
          }));
    if (currentQuestion.visualisueringLink != null &&
        currentQuestion.visualisueringLink != "") {
      spans.add(TextSpan(text: "\nEine Visualisierung findest du "));
      spans.add(TextSpan(
          text: 'hier',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue),
          recognizer: new TapGestureRecognizer()
            ..onTap = () {
              launch(currentQuestion.openDataUrl);
            }));
    }
    if (currentQuestion.imageSrc != null && currentQuestion.imageSrc != "") {
      spans.add(TextSpan(text: "\n\n\nBildquelle:\n"));
      spans.add(
        TextSpan(
          text: currentQuestion.imageSrc,
          style: TextStyle(fontSize: 12),
        ),
      );
    }
    return spans;
  }
}
