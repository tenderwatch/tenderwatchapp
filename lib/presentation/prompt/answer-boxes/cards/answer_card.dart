// taken from https://github.com/jlaskowska/quiz
import 'package:flutter/material.dart';

import 'base_card.dart';

class AnswerCard extends StatelessWidget {
  final bool isSelected;
  final Function onTap;
  final String titleLabel;

  const AnswerCard({
    required this.titleLabel,
    required this.isSelected,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return BaseCard(
      titleLabel: titleLabel,
      borderColor: const Color(0xff204968),
      icon: isSelected ? Icons.check_circle : Icons.circle_outlined,
      iconColor: isSelected ? const Color(0xff2a85e3) : Color(0xff484e71),
      onTap: onTap,
    );
  }
}
