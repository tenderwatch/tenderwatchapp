import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FullScreenImageScreenArgs {
  final ImageProvider imageProvider;

  FullScreenImageScreenArgs(this.imageProvider);
}

class FullScreenImageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context)!.settings.arguments as FullScreenImageScreenArgs;

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      floatingActionButton: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
      body: PhotoView(
        imageProvider: args.imageProvider,
        minScale: PhotoViewComputedScale.contained,
      ),
    );
  }
}
