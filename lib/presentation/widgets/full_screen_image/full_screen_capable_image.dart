import 'package:flutter/material.dart';
import 'package:tenderwatchapp/presentation/widgets/full_screen_image/full_screen_image_screen.dart';
import 'package:tenderwatchapp/routes.dart';

class FullScreenCapableImage extends StatelessWidget {
  final ImageProvider image;

  FullScreenCapableImage({
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(
          Routes.fullScreenImage,
          arguments: FullScreenImageScreenArgs(
            image,
          ),
        );
      },
      child: Image(image: image),
    );
  }
}
