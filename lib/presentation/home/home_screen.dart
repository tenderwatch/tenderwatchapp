import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:tenderwatchapp/actions/actions.dart';
import 'package:tenderwatchapp/app.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/routes.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {

  static Color darken(Color color, [double amount = .1]) {
    assert(amount >= 0 && amount <= 1);

    final hsl = HSLColor.fromColor(color);
    final hslDark = hsl.withLightness((hsl.lightness - amount).clamp(0.0, 1.0));

    return hslDark.toColor();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: TenderWatchAppState.backgroundColor,
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: screenHeight * 0.12,
            ),
            Container(
              height: screenHeight * 0.2,
              child: Image.asset('assets/images/muensterhack-logo.png'),
            ),
            SizedBox(
              height: screenHeight * 0.03,
            ),
            Text(
              'home.app_title',
              style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold,
              ),
            ).tr(),
            SizedBox(
              height: screenHeight * 0.02,
            ),
            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Text(
                  'home.welcome_text',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.center,
                ).tr(),
              ),
            ),
            SizedBox(
              height: screenHeight * 0.07,
            ),
            StoreConnector<AppState,
                void Function(List<QuestionType> validCategories)>(
              converter: (store) => (List<QuestionType> categories) =>
                  store.dispatch(StartQuizAction(categories)),
              builder: (context,
                  void Function(List<QuestionType> categories) startQuiz) {
                return ElevatedButton(
                  onPressed: () {
                    startQuiz([
                      QuestionType.geoguesser,
                      QuestionType.guess_value,
                      QuestionType.single_choice
                    ]);
                    Navigator.of(context).pushNamed(Routes.prompt);
                  },
                  child: Text('home.all_categories_button').tr(),
                );
              },
            ),
            SizedBox(
              height: 25,
            ),
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                primary: darken(TenderWatchAppState.primaryColor, 0.1)
              ),
              child: Text('home.random_question_button').tr(),
            ),
            SizedBox(
              height: screenHeight * 0.2,
            ),
            Center(
                child: TextButton(
              onPressed: () => showLicensePage(context: context),
              child: Text(
                'home.license_button',
                style: TextStyle(
                  fontSize: 20,
                ),
              ).tr(),
            ))
          ],
        ),
      ),
    );
  }
}
