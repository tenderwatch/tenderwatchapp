import 'package:tenderwatchapp/main_lib.dart';
import 'package:tenderwatchapp/models/app_environment.dart';

Future<void> main() async {
  await runWithEnvironment(AppEnvironment.PROD);
}
