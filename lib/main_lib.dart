import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:redux/redux.dart';
import 'package:tenderwatchapp/app.dart';
import 'package:tenderwatchapp/middleware/middlewares.dart';
import 'package:tenderwatchapp/middleware/persistence/hive_storage.dart';
import 'package:tenderwatchapp/models/app_environment.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/reducers/app_state_reducer.dart';
import 'package:tenderwatchapp/services/services_init.dart';
import 'package:tenderwatchapp/util/hive-util.dart';

Future<void> runWithEnvironment(AppEnvironment appEnvironment) async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  registerServices();

  await HiveUtil().init();
  final hiveStorage = HiveStorage();

  // Register licenses
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('assets/g_fonts/LICENSE.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });

  runApp(
    await getApp(
      store: Store<AppState>(
        appReducer,
        initialState: hiveStorage.loadState(appEnvironment),
        middleware: middlewares,
      ),
    ),
  );
}

Future<Widget> getApp({
  required Store<AppState> store,
  Widget? debugWidget,
  GlobalKey<NavigatorState>? navigatorKey,
}) async {
  return EasyLocalization(
    supportedLocales: [
      Locale('de', 'DE'),
    ],
    path: 'assets/translations',
    fallbackLocale: Locale('de', 'DE'),
    startLocale: Locale('de', 'DE'),
    child: TenderWatchApp(
      store: store,
    ),
  );
}
