class Routes {
  static const home = '/';
  static const prompt = '/prompt';
  static const debug = '/debug';
  static const fullScreenImage = '/fullScreenImage';
}
