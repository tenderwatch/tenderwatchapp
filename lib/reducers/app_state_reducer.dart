import 'package:redux/redux.dart';
import 'package:tenderwatchapp/actions/actions.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/reducers/question_state_reducer.dart';

AppState appReducer(AppState state, action) {
  return state.copyWith(
    counter: counterReducer(state.counter, action),
    questionState: questionStateReducer(state.questionState, action),
  );
}

final counterReducer = combineReducers<int>([
  TypedReducer<int, SetCounterAction>(
    _onSetCounter,
  )
]);

int _onSetCounter(int counter, SetCounterAction action) {
  return action.counter;
}
