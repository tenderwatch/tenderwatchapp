import 'package:redux/redux.dart';
import 'package:tenderwatchapp/actions/actions.dart';
import 'package:tenderwatchapp/models/states/question_state.dart';

final questionStateReducer = combineReducers<QuestionState>([
  TypedReducer<QuestionState, SetCurrentQuestionAction>(
    _onSetCurrentQuestion,
  ),
  TypedReducer<QuestionState, StartQuizAction>(
    _onStartQuiz,
  ),
  TypedReducer<QuestionState, SetCurrentQuestionNumAction>(
    _onSetCurrentQuestionNum,
  ),
  TypedReducer<QuestionState, SetCurrentScoreAction>(
    _onSetCurrentScore,
  ),
  TypedReducer<QuestionState, SwitchToNextQuestionAction>(
    _onSwitchToNextQuestion,
  ),
  TypedReducer<QuestionState, SetShowingSolutionAction>(
    _onSetShowingSolution,
  ),
  TypedReducer<QuestionState, SetGuessedLocationAction>(
    _onSetGuessedLocation,
  ),
  TypedReducer<QuestionState, SetSCSelectedAnswerIdxAction>(
    _onSetSelectedAnswerIdx,
  )
]);

QuestionState _onStartQuiz(
    QuestionState questionState, StartQuizAction action) {
  return questionState.copyWith(
    validCategories: action.validCategories,
    currentScore: 0,
    showingSolution: false,
  );
}

QuestionState _onSetShowingSolution(
    QuestionState questionState, SetShowingSolutionAction action) {
  return questionState.copyWith(showingSolution: action.showingSolution);
}

QuestionState _onSwitchToNextQuestion(
    QuestionState questionState, SwitchToNextQuestionAction action) {
  return questionState
      .copyWith(
        currentQuestionNum: action.questionNum,
        showingSolution: false,
      )
      .copyWithNullableGuessedLocation(
          guessedLocation: null, sc_selectedIndex: null);
}

QuestionState _onSetCurrentQuestion(
    QuestionState questionState, SetCurrentQuestionAction action) {
  return questionState.copyWith(currentQuestion: action.currentQuestion);
}

QuestionState _onSetCurrentQuestionNum(
    QuestionState questionState, SetCurrentQuestionNumAction action) {
  return questionState.copyWith(currentQuestionNum: action.questionNum);
}

QuestionState _onSetCurrentScore(
    QuestionState questionState, SetCurrentScoreAction action) {
  return questionState.copyWith(currentScore: action.score);
}

QuestionState _onSetGuessedLocation(
    QuestionState questionState, SetGuessedLocationAction action) {
  return questionState.copyWith(guessedLocation: action.guessedLocation);
}

QuestionState _onSetSelectedAnswerIdx(
    QuestionState questionState, SetSCSelectedAnswerIdxAction action) {
  if (action.selectedIdx == null) {
    return questionState.copyWithNullableGuessedLocation(
        guessedLocation: questionState.guessedLocation, sc_selectedIndex: null);
  }
  return questionState.copyWith(sc_selectedIndex: action.selectedIdx);
}
