import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:redux/redux.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/presentation/debug_screen.dart';
import 'package:tenderwatchapp/presentation/home/home_screen.dart';
import 'package:tenderwatchapp/presentation/prompt/prompt_screen.dart';
import 'package:tenderwatchapp/presentation/widgets/full_screen_image/full_screen_image_screen.dart';
import 'package:tenderwatchapp/routes.dart';

class TenderWatchApp extends StatefulWidget {
  final Store<AppState> store;

  TenderWatchApp({required this.store});

  @override
  State<TenderWatchApp> createState() => TenderWatchAppState(store: store);
}

class TenderWatchAppState extends State<TenderWatchApp> {
  final Store<AppState> store;

  TenderWatchAppState({required this.store});

  static const Color primaryColor = Color(0xFFDA121A);
  static const Color backgroundColor = Color(0xFFFCDD09);

  @override
  Widget build(BuildContext context) {
    final routes = getRoutes();
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'TenderWatch', // Translations are not loaded yet
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: primaryColor,
          primarySwatch: createMaterialColor(Color(0xFFDA121A)),
          textTheme:
              GoogleFonts.shareTechMonoTextTheme(Theme.of(context).textTheme),
        ),
        initialRoute: Routes.home,
        onGenerateRoute: (settings) {
          final builder = routes[settings.name];
          if (builder == null) {
            return null;
          } // Or some error page? Shouldn't happen anyway - this is basically 404
          return buildRoute(builder(), settings);
        },
      ),
    );
  }

  PageRouteBuilder buildRoute(Widget target, RouteSettings settings) {
    return PageRouteBuilder(
      settings: settings,
      pageBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
      ) =>
          target,
      transitionDuration: Duration(milliseconds: 200),
      reverseTransitionDuration: Duration(milliseconds: 200),
      transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
      ) {
        return SlideTransition(
          position: Tween<Offset>(
            begin: const Offset(1, 0),
            end: Offset.zero,
          ).animate(animation),
          child: child,
        );
      },
    );
  }

  static Map<String, Widget Function()> getRoutes() => {
        Routes.home: () => HomeScreen(),
        Routes.prompt: () => PromptScreen(),
        Routes.debug: () => DebugScreen(),
        Routes.fullScreenImage: () => FullScreenImageScreen(),
      };
}

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  final swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}
