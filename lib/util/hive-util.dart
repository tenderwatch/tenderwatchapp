import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/models/states/question_state.dart';

class HiveUtil {
  static final HiveUtil _instance = HiveUtil._internal();

  factory HiveUtil() {
    return _instance;
  }

  Box<AppState>? stateBox; // TODO: @Felix check if nullable is required

  HiveUtil._internal();

  Future<void> init() async {
    await Hive.initFlutter();
    Hive.registerAdapter(AppStateAdapter());
    Hive.registerAdapter(QuestionStateAdapter());
    stateBox = await Hive.openBox<AppState>('state');
  }
}
