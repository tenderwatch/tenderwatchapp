import 'package:get_it/get_it.dart';
import 'package:tenderwatchapp/models/app_environment.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/services/migrations/db_migration_service.dart';
import 'package:tenderwatchapp/util/hive-util.dart';

class HiveStorage {
  void saveState(AppState state) {
    HiveUtil().stateBox!.put('state', state);
  }

  AppState loadState(AppEnvironment env) {
    // TODO: @Felix Wäre cool, wenn Du es hinbekommst, dass die stateBox auf nicht nullable gesetzt werden kann, dann brauchen wir die null checks net
    var storedState = HiveUtil().stateBox!.get('state');

    if (storedState == null) {
      return AppState(appEnvironment: env);
    }

    storedState = GetIt.I.get<DBMigrationService>().migrateState(storedState);

    return storedState.copyWith(appEnvironment: env);
  }
}
