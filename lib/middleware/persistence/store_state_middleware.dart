import 'package:redux/redux.dart';
import 'package:tenderwatchapp/actions/saveable_action.dart';
import 'package:tenderwatchapp/middleware/persistence/hive_storage.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';

List<Middleware<AppState>> storeMiddlewares = [
  TypedMiddleware<AppState, SaveableAction>(_saveState),
];

void _saveState(
    Store<AppState> store, SaveableAction action, NextDispatcher next) {
  next(action);

  if (action.saveStateAfter) {
    HiveStorage().saveState(store.state);
  }
}
