import 'package:redux/redux.dart';
import 'package:tenderwatchapp/middleware/persistence/store_state_middleware.dart';
import 'package:tenderwatchapp/middleware/question/question_middleware.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';

List<Middleware<AppState>> middlewares = [
  ...storeMiddlewares,
  ...questionMiddlewares,
];
