import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:tenderwatchapp/actions/actions.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/services/questions/question_service.dart';

List<Middleware<AppState>> questionMiddlewares = [
  TypedMiddleware<AppState, SwitchToNextQuestionAction>(
      _onSwitchToNextQuestion),
  TypedMiddleware<AppState, StartQuizAction>(_onStartQuiz),
];

void _onSwitchToNextQuestion(Store<AppState> store,
    SwitchToNextQuestionAction action, NextDispatcher next) {
  next(action);

  final questionService = GetIt.I.get<QuestionService>();
  final newQuestion = questionService.allQuestions[action.questionNum - 1];

  store.dispatch(SetCurrentQuestionAction(newQuestion));
}

void _onStartQuiz(
    Store<AppState> store, StartQuizAction action, NextDispatcher next) {
  next(action);
  GetIt.I.get<QuestionService>().allQuestions.shuffle();
  store.dispatch(SwitchToNextQuestionAction(1));
}
