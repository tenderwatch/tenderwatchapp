import 'package:tenderwatchapp/models/states/app_state.dart';

AppState migrateFrom0(AppState state) {
  return state.copyWith(dbVersionNum: 1);
}
