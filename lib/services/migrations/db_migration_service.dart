import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/services/migrations/migrations/migration_null.dart';

class DBMigrationService {
  // Add new migrations here!
  Map<int, AppState Function(AppState)> migrationsMap = {
    0: migrateFrom0,
  };

  AppState migrateState(AppState storedState) {
    while (storedState.dbVersionNum < AppState.CUR_DB_VERSION_NUM) {
      if (migrationsMap[storedState.dbVersionNum] == null) {
        return storedState; // Should not happen, see db_migration_service_test.dart
      }

      storedState = migrationsMap[storedState.dbVersionNum]!(storedState);
    }

    return storedState;
  }
}
