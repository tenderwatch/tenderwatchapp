import 'dart:math';

import 'package:latlng/latlng.dart';
import 'package:tenderwatchapp/models/data/question_types/geoguesser_question.dart';
import 'package:tenderwatchapp/services/questions/question_types/question_service_template.dart';

class GeoguesserQuestionService implements QuestionServiceTemplate {
  List<GeoguesserQuestion> getQuestions() {
    return [
      GeoguesserQuestion(
        category: 'Finde den Ort',
        openDataUrl: 'https://upload.wikimedia.org/wikipedia/commons/8/80/M%C3%BCnster%2C_Erbdrostenhof_--_2020_--_6745.jpg',
        questionText: 'Wo befindet sich dieses bekannte Gebäude?',
        image: 'https://upload.wikimedia.org/wikipedia/commons/8/80/M%C3%BCnster%2C_Erbdrostenhof_--_2020_--_6745.jpg',
        coords: LatLng(51.961238, 7.632125),
      ),
    ];
  }

  GeoguesserQuestion getRandomQuestion() {
    return getQuestions()[Random().nextInt(getQuestions().length)];
  }
}
