import 'dart:math';

import 'package:tenderwatchapp/models/data/question_types/single_choice_question.dart';
import 'package:tenderwatchapp/services/questions/question_types/question_service_template.dart';

class SingleChoiceQuestionService implements QuestionServiceTemplate {
  SingleChoiceQuestion shuffleAnswers(
      SingleChoiceQuestion singleChoiceQuestion) {
    List<MapEntry<int, String>> newOrder =
        singleChoiceQuestion.options.asMap().entries.toList();
    newOrder.shuffle();
    var newCorrectAnswer = -1;
    for (int i = 0; i < newOrder.length; i++) {
      var e = newOrder[i];
      if (e.key == singleChoiceQuestion.correct_answer) {
        newCorrectAnswer = i;
        break;
      }
    }
    assert(newCorrectAnswer != -1);
    var newOptions = newOrder.map((e) => e.value).toList();
    assert(newOptions[newCorrectAnswer] ==
        singleChoiceQuestion.options[singleChoiceQuestion.correct_answer]);

    // update Values
    return SingleChoiceQuestion(
        category: singleChoiceQuestion.category,
        openDataUrl: singleChoiceQuestion.openDataUrl,
        questionText: singleChoiceQuestion.questionText,
        image: singleChoiceQuestion.image,
        options: newOptions,
        correct_answer: newCorrectAnswer);
  }

  List<SingleChoiceQuestion> getQuestions() {
    return all_questions.map((e) => shuffleAnswers(e)).toList();
  }

  SingleChoiceQuestion getRandomQuestion() {
    return getQuestions()[Random().nextInt(getQuestions().length)];
  }

  final List<SingleChoiceQuestion> all_questions = [
    new SingleChoiceQuestion(
      category: 'Sport und Freizeit',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/sporthallen-belegungsplan',
      questionText:
          'Wer belegt dienstags um 18 Uhr am Annete-von-Droste-Hülshoff-Gymnasium die Sporthalle?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/d/d6/Muenster_annettegymnasium_schulhof_01.jpg',
      imageSrc:
          'No machine-readable author provided. Hermans~commonswiki assumed (based on copyright claims). (https://commons.wikimedia.org/wiki/File:Muenster_annettegymnasium_schulhof_01.jpg), „Muenster annettegymnasium schulhof 01“, https://creativecommons.org/licenses/by-sa/3.0/legalcode',
      options: [
        'Ski-Club Münster',
        'Chi-Dao Kampfkunst Münster',
        'Betriebssportverband Münster'
      ],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Sport und Freizeit',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/sporthallen-belegungsplan',
      questionText:
          'Welche Veranstaltung wurde während des Ideenpitches (freitags 9 Uhr) in der Sporthalle des Paulinum angeboten?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/1/1f/Muenster_Paulinum_4608.jpg',
      imageSrc:
          'Rüdiger Wölk This photo was taken by Rüdiger Wölk. Please credit this photo Rüdiger Wölk, Münster. View all photos (large page) of Rüdiger Wölk I would also appreciate an email to rudiger.wolk@gmail.com with details of use. Für Hinweise auf Veröffentlichungen (rudiger.wolk@gmail.com) oder Belegexemplare bin ich Ihnen dankbar. (https://commons.wikimedia.org/wiki/File:Muenster_Paulinum_4608.jpg), „Muenster Paulinum 4608“, https://creativecommons.org/licenses/by-sa/2.5/legalcode',
      options: ['Schulsport', 'Kinderturnen', 'Tanzkurs'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Sport und Freizeit',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/sporthallen-belegungsplan',
      questionText:
          'Wer hat während des Abschlusspitches (ab 18 Uhr) die Dreifachhalle des Hansa-Berufkollegs reserviert?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Hansaring_80_Handelslehranstalt_A_IMG_1700.jpg/1280px-Hansaring_80_Handelslehranstalt_A_IMG_1700.jpg',
      imageSrc:
          'RoxFuchs (https://commons.wikimedia.org/wiki/File:Hansaring_80_Handelslehranstalt_A_IMG_1700.jpg), „Hansaring 80 Handelslehranstalt A IMG 1700“, https://creativecommons.org/licenses/by-sa/4.0/legalcode',
      options: [
        'TTC Münster',
        'TuS Saxonia Münster',
        'Sportclub Münster 08'
      ],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Sport und Freizeit',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/sporthallen-belegungsplan',
      questionText:
          'Welche Sportart wurde heute morgen während unseres gemeinsamen Frühstückes in der Sporthalle der Hans-Böckler-Schule betrieben?',
      image: '', // TODO image
      options: [
        'Handball',
        'Fußball',
      ],
      correct_answer: 0,
    ),
    // Münstereraner Straßen
    new SingleChoiceQuestion(
      category: 'Müsteraner Straßen',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/stra%C3%9Fenliste/resource/722c24df-3477-4c6e-801d-d1dc55e36d64',
      questionText: 'Welche Straßen gibt es in Münster nicht?',
      image: '',
      options: ['Unterwasserstraße', 'Überwasserstraße', 'Adelword'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Müsteraner Straßen',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/stra%C3%9Fenliste/resource/722c24df-3477-4c6e-801d-d1dc55e36d64',
      questionText: 'Welche Straßen gibt es in Münster nicht?',
      image: '',
      options: ['Wolfskuhle', 'Steinkuhle', 'Dängsel'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Müsteraner Straßen',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/stra%C3%9Fenliste/resource/722c24df-3477-4c6e-801d-d1dc55e36d64',
      questionText: 'Welche Straßen gibt es in Münster nicht?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/a/ab/Bushaltestelle_Von-Steuben-Stra%C3%9Fe%2C_1%2C_Mitte%2C_M%C3%BCnster.jpg',
      imageSrc:
          'GeorgDerReisende (https://commons.wikimedia.org/wiki/File:Bushaltestelle_Von-Steuben-Straße,_1,_Mitte,_Münster.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode ',
      options: ['Kugelskamp', 'Langebusch', 'Mausbachstraße'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Müsteraner Straßen',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/stra%C3%9Fenliste/resource/722c24df-3477-4c6e-801d-d1dc55e36d64',
      questionText: 'Welche Straßen gibt es in Münster nicht?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/a/ab/Bushaltestelle_Von-Steuben-Stra%C3%9Fe%2C_1%2C_Mitte%2C_M%C3%BCnster.jpg',
      imageSrc:
          'GeorgDerReisende (https://commons.wikimedia.org/wiki/File:Bushaltestelle_Von-Steuben-Straße,_1,_Mitte,_Münster.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode ',
      options: ['Mampfingstraße', 'Mümmelmannpfad', 'Hummelbrink'],
      correct_answer: 0,
    ),
    // Top-Name
    new SingleChoiceQuestion(
      category: 'Vornamen Münster',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/vornamenstatistik-f%C3%BCr-neugeborene-nach-geburtsjahr-m%C3%BCnster/',
      questionText:
          'Welche der drei Vornamen Ralf, Sebastian und Alexander war in den letzten 10 Jahren in den Top 30 der Baby-Namen?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg/1280px-Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg',
      imageSrc:
          'Franziskus Münster (https://commons.wikimedia.org/wiki/File:Eingang_St._Franziskus-Hospital_Münster_.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode',
      options: ['Alexander', 'Ralf', 'Sebastian'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Vornamen Münster',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/vornamenstatistik-f%C3%BCr-neugeborene-nach-geburtsjahr-m%C3%BCnster/',
      questionText:
          'Welche der drei Vornamen Karin, Pia und Ludger war in den letzten 10 Jahren in den Top 30 der Baby-Namen?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg/1280px-Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg',
      imageSrc:
          'Franziskus Münster (https://commons.wikimedia.org/wiki/File:Eingang_St._Franziskus-Hospital_Münster_.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode',
      options: ['Pia', 'Markus', 'Ludger'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Vornamen Münster',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/vornamenstatistik-f%C3%BCr-neugeborene-nach-geburtsjahr-m%C3%BCnster/',
      questionText:
          'Welche der drei Vornamen Camilla, Andre und Maximilian war in den letzten 10 Jahren in den Top 30 der Baby-Namen?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg/1280px-Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg',
      imageSrc:
          'Franziskus Münster (https://commons.wikimedia.org/wiki/File:Eingang_St._Franziskus-Hospital_Münster_.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode',
      options: ['Jonas', 'Oezbay', 'Sebastian'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Vornamen Münster',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/vornamenstatistik-f%C3%BCr-neugeborene-nach-geburtsjahr-m%C3%BCnster/',
      questionText:
          'Was ist der beliebteste Vorname für Mädchen in 2020 in Münster?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg/1280px-Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg',
      imageSrc:
          'Franziskus Münster (https://commons.wikimedia.org/wiki/File:Eingang_St._Franziskus-Hospital_Münster_.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode',
      options: ['Emilia', 'Gudula', 'Annika'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Vornamen Münster',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/vornamenstatistik-f%C3%BCr-neugeborene-nach-geburtsjahr-m%C3%BCnster/',
      questionText:
          'Was ist der beliebteste Vorname für Jungen im Jahr 2007 in Münster?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg/1280px-Eingang_St._Franziskus-Hospital_M%C3%BCnster_.jpg',
      imageSrc:
          'Franziskus Münster (https://commons.wikimedia.org/wiki/File:Eingang_St._Franziskus-Hospital_Münster_.jpg), https://creativecommons.org/licenses/by-sa/4.0/legalcode',
      options: ['Leon', 'Max', 'Paul'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Diverses',
      openDataUrl: '', // TODO: Add open data url to single choice question
      questionText: 'Wie viele Open Data Quellen hat Münster?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Open_Data_stickers.jpg/800px-Open_Data_stickers.jpg',
      imageSrc:
          ' Jonathan Gray (https://commons.wikimedia.org/wiki/File:Open_Data_stickers.jpg), „Open Data stickers“, https://creativecommons.org/publicdomain/zero/1.0/legalcode ',
      options: ['398', '272', '128'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Wohnungsfertigstellungen und Abbrüche',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
      questionText:
          'Wie viele Wohnungen wurden in Hiltrup im Jahr 2019 fertigestellt?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Rheinpanorama_1856_detail_Dom.jpg/672px-Rheinpanorama_1856_detail_Dom.jpg',
      imageSrc:
          'Johann Franz Michiels creator QS:P170,Q18508387 (https://commons.wikimedia.org/wiki/File:Rheinpanorama_1856_detail_Dom.jpg), „Rheinpanorama 1856 detail Dom“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
      options: ['199', '324', '63'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Wohnungsfertigstellungen und Abbrüche',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
      questionText:
          'Wie viele Wohnungsabbrüche gab es in Hiltrup im Jahr 2019?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Rheinpanorama_1856_detail_Dom.jpg/672px-Rheinpanorama_1856_detail_Dom.jpg',
      imageSrc:
          'Johann Franz Michiels creator QS:P170,Q18508387 (https://commons.wikimedia.org/wiki/File:Rheinpanorama_1856_detail_Dom.jpg), „Rheinpanorama 1856 detail Dom“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
      options: ['27', '3', '45'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Wohnungsfertigstellungen und Abbrüche',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
      questionText:
          'Wie viele 2-Raum-Wohnungen wurden im Jahr 2018 in Mauritz-West fertiggestellt?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Rheinpanorama_1856_detail_Dom.jpg/672px-Rheinpanorama_1856_detail_Dom.jpg',
      imageSrc:
          'Johann Franz Michiels creator QS:P170,Q18508387 (https://commons.wikimedia.org/wiki/File:Rheinpanorama_1856_detail_Dom.jpg), „Rheinpanorama 1856 detail Dom“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
      options: ['6', '80', '14'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Wohnungsfertigstellungen und Abbrüche',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Rheinpanorama_1856_detail_Dom.jpg/672px-Rheinpanorama_1856_detail_Dom.jpg',
      imageSrc:
          'Johann Franz Michiels creator QS:P170,Q18508387 (https://commons.wikimedia.org/wiki/File:Rheinpanorama_1856_detail_Dom.jpg), „Rheinpanorama 1856 detail Dom“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
      questionText:
          'Wie viele 5- und Mehr-Raum-Wohnungen wurden im Jahr 2019 in Kinderhaus fertiggestellt?',
      options: ['17', '34', '8'],
      correct_answer: 0,
    ),
    new SingleChoiceQuestion(
      category: 'Wohnungsfertigstellungen und Abbrüche',
      openDataUrl: 'https://opendata.stadt-muenster.de/dataset/statistik-wohnungsbaut%C3%A4tigkeit',
      questionText:
          'Wie viele 3-Raum-Wohnungen wurden im Jahr 2019 in Roxel fertiggestellt?',
      image:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Rheinpanorama_1856_detail_Dom.jpg/672px-Rheinpanorama_1856_detail_Dom.jpg',
      imageSrc:
          'Johann Franz Michiels creator QS:P170,Q18508387 (https://commons.wikimedia.org/wiki/File:Rheinpanorama_1856_detail_Dom.jpg), „Rheinpanorama 1856 detail Dom“, marked as public domain, more details on Wikimedia Commons: https://commons.wikimedia.org/wiki/Template:PD-old',
      options: ['22', '59', '84'],
      correct_answer: 0,
    ),
  ];
}
