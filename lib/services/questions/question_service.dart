import 'dart:math';

import 'package:get_it/get_it.dart';
import 'package:tenderwatchapp/models/data/question_types/guess_question.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';
import 'package:tenderwatchapp/services/questions/question_types/geoguesser_question_service.dart';
import 'package:tenderwatchapp/services/questions/question_types/guess_question_service.dart';
import 'package:tenderwatchapp/services/questions/question_types/question_service_template.dart';
import 'package:tenderwatchapp/services/questions/question_types/single_choice_question_service.dart';

class QuestionService {
  Map<QuestionType, QuestionServiceTemplate> _questionServices = {
    QuestionType.geoguesser: GetIt.I.get<GeoguesserQuestionService>(),
    QuestionType.guess_value: GetIt.I.get<GuessQuestionService>(),
    QuestionType.single_choice: GetIt.I.get<SingleChoiceQuestionService>(),
  };

  late final List<Question> allQuestions;

  QuestionService() {
    allQuestions = _questionServices.values.fold(
        [], (questions, service) => questions..addAll(service.getQuestions()));
  }

  Question getRandomQuestion(List<QuestionType> validCategories) {
    return _questionServices[
            validCategories[Random().nextInt(validCategories.length)]]!
        .getRandomQuestion();
  }
}
