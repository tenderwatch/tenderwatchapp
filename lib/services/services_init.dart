import 'package:get_it/get_it.dart';
import 'package:tenderwatchapp/services/migrations/db_migration_service.dart';
import 'package:tenderwatchapp/services/questions/question_types/geoguesser_question_service.dart';
import 'package:tenderwatchapp/services/questions/question_service.dart';
import 'package:tenderwatchapp/services/questions/question_types/guess_question_service.dart';
import 'package:tenderwatchapp/services/questions/question_types/single_choice_question_service.dart';

final getIt = GetIt.I;

void registerServices() {
  getIt.registerLazySingleton<DBMigrationService>(() => DBMigrationService());
  getIt.registerLazySingleton<QuestionService>(() => QuestionService());
  getIt.registerLazySingleton<GuessQuestionService>(
      () => GuessQuestionService());
  getIt.registerLazySingleton<GeoguesserQuestionService>(
      () => GeoguesserQuestionService());
  getIt.registerLazySingleton<SingleChoiceQuestionService>(
      () => SingleChoiceQuestionService());
}
