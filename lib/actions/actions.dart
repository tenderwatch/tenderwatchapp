import 'package:latlng/latlng.dart';
import 'package:tenderwatchapp/actions/saveable_action.dart';
import 'package:tenderwatchapp/models/data/question_types/question.dart';

class SetCounterAction extends SaveableAction {
  final int counter;

  SetCounterAction(this.counter) : super(saveStateAfter: true);
}

class SetCurrentQuestionAction extends SaveableAction {
  final Question? currentQuestion;

  SetCurrentQuestionAction(this.currentQuestion) : super(saveStateAfter: true);
}

class SwitchToNextQuestionAction extends SaveableAction {
  final int questionNum;

  SwitchToNextQuestionAction(this.questionNum) : super(saveStateAfter: true);
}

class StartQuizAction extends SaveableAction {
  final List<QuestionType> validCategories;

  StartQuizAction(this.validCategories) : super(saveStateAfter: true);
}

class SetCurrentScoreAction extends SaveableAction {
  final double score;

  SetCurrentScoreAction(this.score) : super(saveStateAfter: true);
}

class SetCurrentQuestionNumAction extends SaveableAction {
  final int questionNum;

  SetCurrentQuestionNumAction(this.questionNum) : super(saveStateAfter: true);
}

class SetShowingSolutionAction extends SaveableAction {
  final bool showingSolution;

  SetShowingSolutionAction(this.showingSolution) : super(saveStateAfter: true);
}

class SetGuessedLocationAction extends SaveableAction {
  final LatLng? guessedLocation;

  SetGuessedLocationAction(this.guessedLocation) : super(saveStateAfter: true);
}

class SetSCSelectedAnswerIdxAction extends SaveableAction {
  final int? selectedIdx;

  SetSCSelectedAnswerIdxAction(this.selectedIdx) : super(saveStateAfter: true);
}
