import 'package:flutter_test/flutter_test.dart';
import 'package:tenderwatchapp/models/states/app_state.dart';
import 'package:tenderwatchapp/services/migrations/db_migration_service.dart';

void main() {
  testWidgets('All migrations exist', (WidgetTester tester) async {
    final migrationService = DBMigrationService();

    for (var i = 1; i < AppState.CUR_DB_VERSION_NUM; i++) {
      expect(isNotNull, migrationService.migrationsMap[i]);
    }
  });
}
