get_deps:
	flutter pub get

generate: get_deps
	flutter packages pub run build_runner build

build_android: generate
	flutter build apk

build_web: generate
	flutter build web
	cp -r build/web public 

serve_web: 
	flutter run -d chrome

build_web_gitlab: generate 
	flutter build web --base-href="/tenderwatchapp/" 
	cp -r build/web public 
